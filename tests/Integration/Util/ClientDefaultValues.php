<?php
declare(strict_types=1);

namespace App\Tests\Integration\Util;

class ClientDefaultValues
{
    public static function getDefaultConfiguration(): array
    {
        return
            [
                'base_uri' => getenv('URL_DOMAIN'),
                'headers'  => [
                    'accept' => [
                        'application/json',
                    ],
                ],
            ];
    }
}
