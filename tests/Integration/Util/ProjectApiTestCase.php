<?php
declare(strict_types=1);

namespace App\Tests\Integration\Util;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ProjectApiTestCase extends ApiTestCase
{
    public ?string $token = null;

    public ?Client $client = null;

    public ?Serializer $serializer = null;

    public ?EntityManagerInterface $entityManager;

    protected function setUp()
    {
        $this->initializeClient();
        $this->initializeSerializer();
        $this->entityManager = $this->client->getContainer()
                                            ->get('doctrine.orm.entity_manager');

    }

    protected function createTransaction(): void
    {
        $this->entityManager->beginTransaction();
        $this->entityManager->getConnection()
                            ->setAutoCommit(false);
    }

    protected function rollback(): void
    {
        $this->entityManager->rollback();
    }

    protected function initializeClient(): void
    {
        $this->client = static::createClient(
            [
            ],
            ClientDefaultValues::getDefaultConfiguration()
        );
        $this->client->disableReboot();
    }

    private function initializeSerializer()
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $this->serializer = new Serializer($normalizers, $encoders);
    }
}
