<?php
declare(strict_types=1);

namespace App\Tests\Integration\Api\Product;

use App\Domain\Product\Product;
use App\Tests\Integration\Util\ProjectApiTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class ProductApiTest extends ProjectApiTestCase
{
    const DESCRIPTION
                         = 'more_than_100_letters_string_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
           more_than_100_letters_string_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
           more_than_100_letters_string_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
           more_than_100_letters_string_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
    const PRICE_VALUE    = 123.123;
    const PRICE_CURRENCY = 'PLN';
    const REQUIRED_DATA
        = [
            'description'   => self::DESCRIPTION,
            'priceValue'    => self::PRICE_VALUE,
            'priceCurrency' => self::PRICE_CURRENCY,
        ];

    public function test_Should_CreateProduct_WhenRequiredDataIsGiven(): void
    {
        // GIVEN
        $this->createTransaction();
        // WHEN
        $this->client->request(
            'POST',
            'api/product',
            [
                'json' => self::REQUIRED_DATA,
            ]
        );
        // THEN
        self::assertResponseIsSuccessful();
        $this->rollback();
    }

    public function test_Should_ReturnError_When_DescriptionIsToShort(): void
    {
        // GIVEN
        $this->createTransaction();
        // WHEN
        $this->client->request(
            'POST',
            'api/product',
            [
                'json' => array_merge(
                    self::REQUIRED_DATA,
                    ['description' => 'test']
                ),
            ]
        );
        // THEN
        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
        $this->rollback();
    }

    public function test_Should_ReturnError_When_PriceValueMissing(): void
    {
        // GIVEN
        $this->createTransaction();
        // WHEN
        $this->client->request(
            'POST',
            'api/product',
            [
                'json' => array_merge(
                    self::REQUIRED_DATA,
                    ['priceValue' => null]
                ),
            ]
        );
        // THEN
        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
        $this->rollback();
    }

    public function test_Should_ReturnError_When_PriceCurrencyMissing(): void
    {
        // GIVEN
        $this->createTransaction();
        // WHEN
        $this->client->request(
            'POST',
            'api/product',
            [
                'json' => array_merge(
                    self::REQUIRED_DATA,
                    ['priceCurrency' => null]
                ),
            ]
        );
        // THEN
        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
        $this->rollback();
    }

    public function test_Should_ReturnAllProducts(): void
    {
        // GIVEN
        $productInDatabaseQuantity = sizeof(
            $this->entityManager->getRepository(Product::class)
                                ->findAll()
        );
        // WHEN
        $response = $this->client->request(
            'GET',
            'api/product'
        );

        // THEN
        $decodedResponse = $this->serializer->decode($response->getContent(), JsonEncoder::FORMAT);
        $productFromResponseQuantity = sizeof($decodedResponse);
        $this->assertEquals($productInDatabaseQuantity, $productFromResponseQuantity);
    }

    public function test_Should_ReturnGivenQuantityOfItems(): void
    {
        // GIVEN
        $itemsPerPage = 10;
        // WHEN
        $response = $this->client->request(
            'GET',
            'api/product?itemsPerPage='.$itemsPerPage
        );

        // THEN
        $decodedResponse = $this->serializer->decode($response->getContent(), JsonEncoder::FORMAT);
        $productFromResponseQuantity = sizeof($decodedResponse);
        $this->assertEquals($itemsPerPage, $productFromResponseQuantity);
    }
}
