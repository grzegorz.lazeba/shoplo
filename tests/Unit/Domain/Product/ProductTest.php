<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Product;

use App\Application\EventDispatcherInterface;
use App\Application\Product\CreateProductService;
use App\Application\Product\ProductEventFactoryInterface;
use App\Domain\Product\Product;
use App\Domain\Product\ProductId;
use App\Domain\Product\ProductRepositoryInterface;
use Assert\InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ProductTest extends TestCase
{
    public CreateProductService $createProductService;

    private $eventDispatcherMock;

    protected function setUp()
    {
        $productRepositoryMock = $this->createMock(ProductRepositoryInterface::class);
        $productRepositoryMock->method('nextIdentity')->willReturn(
            ProductId::fromString(Uuid::uuid4()->toString())
        );

        $productEventFactoryMock = $this->createMock(ProductEventFactoryInterface::class);
        $this->eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);

        $this->createProductService = new CreateProductService(
            $productRepositoryMock,
            $this->eventDispatcherMock,
            $productEventFactoryMock
        );
    }

    public function test_Should_CreateProduct_When_ValidDataIsGiven(): void
    {
        // GIVEN
        $createProductDTO = new ICanCreateProductMock(
            'more_than_100_letters_string_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
           more_than_100_letters_string_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
           more_than_100_letters_string_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
           more_than_100_letters_string_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            123.123,
        'PLN'
        );
        // WHEN
        $product = $this->createProductService->create($createProductDTO);
        // THEN
        $this->assertInstanceOf(Product::class, $product);
    }

    public function test_Should_ThrowException_When_DescriptionIsTooShort(): void
    {
        // GIVEN
        $this->expectException(InvalidArgumentException::class);

        $createProductDTO = new ICanCreateProductMock(
            'less_than_100_letters_string',
            123.123,
            'PLN'
        );
        // WHEN
        $this->createProductService->create($createProductDTO);
    }

    public function test_Should_DispatchEvent_When_ProductIsCreated(): void
    {
        // GIVEN
        $createProductDTO = new ICanCreateProductMock(
            'more_than_100_letters_string_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
           more_than_100_letters_string_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
           more_than_100_letters_string_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
           more_than_100_letters_string_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            123.123,
            'PLN'
        );

        $this
            ->eventDispatcherMock
            ->expects($this->once())
            ->method('dispatchEvent')
        ;

        // WHEN
         $this->createProductService->create($createProductDTO);
    }

}
