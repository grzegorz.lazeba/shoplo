<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Product;

use App\Application\Product\ICanCreateProduct;

class ICanCreateProductMock implements ICanCreateProduct
{
    public string $description;

    public float $priceValue;

    public string $priceCurrency;

    public function __construct(
        string $description,
        float $priceValue,
        string $priceCurrency
    ) {
        $this->description = $description;
        $this->priceValue = $priceValue;
        $this->priceCurrency = $priceCurrency;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getPriceValue(): float
    {
        return $this->priceValue;
    }

    public function getPriceCurrency(): string
    {
        return $this->priceCurrency;
    }
}
