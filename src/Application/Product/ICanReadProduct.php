<?php
declare(strict_types=1);

namespace App\Application\Product;

interface ICanReadProduct
{
    public function setId(string $id): void;
    public function setDescription(string $description): void;
    public function setPriceValue(float $priceValue): void;
    public function setPriceCurrency(string $priceCurrency): void;
    public function setCreatedAt(\DateTime $createdAt): void;
}
