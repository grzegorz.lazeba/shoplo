<?php
declare(strict_types=1);

namespace App\Application\Product;

use App\Domain\Product\Product;
use App\Application\Event;

interface ProductEventFactoryInterface
{
    public function created(Product $product): Event;
}
