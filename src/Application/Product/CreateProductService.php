<?php
declare(strict_types=1);

namespace App\Application\Product;

use App\Application\EventDispatcherInterface;
use App\Domain\Product\ProductDescription;
use App\Domain\Product\ProductRepositoryInterface;
use App\Domain\Product\Product;
use App\Domain\Product\ProductPrice;

class CreateProductService
{
    private ProductRepositoryInterface $productRepository;

    private EventDispatcherInterface $eventDispatcher;

    private ProductEventFactoryInterface $productEventFactory;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        EventDispatcherInterface $eventDispatcher,
        ProductEventFactoryInterface $productEventFactory
    ) {
        $this->productRepository = $productRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->productEventFactory = $productEventFactory;
    }

    public function create(
        ICanCreateProduct $productInterface
    ): Product {
        $productId = $this->productRepository->nextIdentity();

        $product = Product::createFrom(
            $productId,
            ProductDescription::createFrom($productInterface->getDescription()),
            ProductPrice::createFrom($productInterface->getPriceValue(), $productInterface->getPriceCurrency())
        );
        $this->productRepository->save($product);

        $event = $this->productEventFactory->created($product);
        $this->eventDispatcher->dispatchEvent($event);

        return $product;
    }
}
