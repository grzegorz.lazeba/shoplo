<?php
declare(strict_types=1);

namespace App\Application\Product;

interface ICanCreateProduct
{
    public function getDescription(): string;
    public function getPriceValue(): float;
    public function getPriceCurrency(): string;
}
