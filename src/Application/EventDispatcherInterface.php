<?php
declare(strict_types=1);
namespace App\Application;

interface EventDispatcherInterface
{
    public function dispatchEvent(Event $event);
}
