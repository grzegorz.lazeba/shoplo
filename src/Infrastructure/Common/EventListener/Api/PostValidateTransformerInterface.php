<?php
declare(strict_types=1);

namespace App\Infrastructure\Common\EventListener\Api;

use Symfony\Component\HttpFoundation\Request;

interface PostValidateTransformerInterface
{
    public function supportTransformation($payload, Request $request);
}
