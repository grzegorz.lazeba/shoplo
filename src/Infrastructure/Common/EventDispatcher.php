<?php
declare(strict_types=1);

namespace App\Infrastructure\Common;

use App\Application\Event;
use App\Application\EventDispatcherInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface as SymfonyEventDispatcherInterface;

class EventDispatcher implements EventDispatcherInterface
{
    private SymfonyEventDispatcherInterface $eventDispatcher;

    public function __construct(
        SymfonyEventDispatcherInterface $eventDispatcher
    )
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function dispatchEvent(Event $event)
    {
        $this->eventDispatcher->dispatch($event);
    }
}
