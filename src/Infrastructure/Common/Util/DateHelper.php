<?php
declare(strict_types=1);

namespace App\Infrastructure\Common\Util;

class DateHelper
{
    const DEFAULT_FORMAT = 'Y-m-d H:i:s';
}
