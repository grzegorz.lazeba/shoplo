<?php
declare(strict_types=1);

namespace App\Infrastructure\Product\EventListener\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Application\Product\CreateProductService;
use App\Infrastructure\Product\DTO\Api\Input\ProductInput;
use App\Infrastructure\Common\EventListener\Api\PostValidateTransformerInterface;
use Assert\InvalidArgumentException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use ApiPlatform\Core\Validator\Exception\ValidationException;
use ApiPlatform\Core\Exception\FilterValidationException;

class ProductPostValidateTransformer implements EventSubscriberInterface, PostValidateTransformerInterface
{
    private CreateProductService $createProductService;

    public function __construct(
        CreateProductService $createProductService
    ) {
        $this->createProductService = $createProductService;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['transform', EventPriorities::PRE_WRITE],
        ];
    }

    public function supportTransformation($payload, Request $request)
    {
        return $payload instanceof ProductInput && $request->getMethod() === Request::METHOD_POST;
    }

    public function transform(ViewEvent $viewEvent): void
    {
        /** @var ProductInput $payload */
        $payload = $viewEvent->getControllerResult();
        if (! $this->supportTransformation($payload, $viewEvent->getRequest())) {
            return;
        }
        try {
            $entity = $this->createProductService->create($payload);
        } catch (InvalidArgumentException $invalidArgumentException) {
            throw new FilterValidationException([], $invalidArgumentException->getMessage());
        }
        $viewEvent->setControllerResult($entity);
    }
}
