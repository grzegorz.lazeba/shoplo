<?php
declare(strict_types=1);

namespace App\Infrastructure\Product\EventListener\Mail;

use App\Infrastructure\Product\Event\ProductCreatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProductMailEventListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            ProductCreatedEvent::class => 'sendEmail',
        ];
    }

    public function sendEmail(ProductCreatedEvent $event): void
    {
        //TODO: Implement mail sending
    }
}
