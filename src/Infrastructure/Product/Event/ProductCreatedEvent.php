<?php
declare(strict_types=1);

namespace App\Infrastructure\Product\Event;

use App\Application\Event as DomainEvent;
use App\Domain\Product\Product;
use Symfony\Contracts\EventDispatcher\Event;

class ProductCreatedEvent extends Event implements DomainEvent
{
    public Product $product;

    public function __construct(
        Product $product
    ) {
        $this->product = $product;
    }
}
