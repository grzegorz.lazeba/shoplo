<?php
declare(strict_types=1);

namespace App\Infrastructure\Product\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Infrastructure\Product\DTO\Api\Output\ProductOutput;
use App\Domain\Product\Product;

class ProductDataTransformer implements DataTransformerInterface
{
    /**
     * @param Product $object
     */
    public function transform($object, string $to, array $context = [])
    {
        return ProductOutput::createFromEntity($object);
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return ProductOutput::class === $to && $data instanceof Product;
    }
}
