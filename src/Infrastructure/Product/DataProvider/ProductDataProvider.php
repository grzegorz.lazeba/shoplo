<?php
declare(strict_types=1);

namespace App\Infrastructure\Product\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\ContextAwareQueryResultCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Domain\Product\Product;
use App\Infrastructure\Product\Repository\ProductRepository;

class ProductDataProvider implements
    ItemDataProviderInterface,
    RestrictedDataProviderInterface,
    ContextAwareCollectionDataProviderInterface
{
    private ProductRepository $productRepository;

    private iterable $collectionExtensions;

    public function __construct(
        ProductRepository $productRepository,
        iterable $collectionExtensions
    )
    {
        $this->productRepository = $productRepository;
        $this->collectionExtensions = $collectionExtensions;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = [])
    {
        $queryBuilder = $this->productRepository->createQueryBuilder('p');

        foreach ($this->collectionExtensions as $extension) {
            $extension->applyToCollection(
                $queryBuilder,
                new QueryNameGenerator(),
                $resourceClass,
                $operationName,
                $context
            );
            if ($extension instanceof ContextAwareQueryResultCollectionExtensionInterface
                && $extension->supportsResult($resourceClass, $operationName, $context)) {
                return $extension->getResult($queryBuilder, $resourceClass, $operationName, $context);
            }
        }
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        return $this->productRepository->find($id);
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Product::class == $resourceClass && strtoupper($context["collection_operation_name"]) === 'GET';
    }
}
