<?php
declare(strict_types=1);

namespace App\Infrastructure\Product\EventFactory;

use App\Application\Event;
use App\Application\Product\ProductEventFactoryInterface;
use App\Domain\Product\Product;
use App\Infrastructure\Product\Event\ProductCreatedEvent;

class ProductEventFactory implements ProductEventFactoryInterface
{
    public function created(Product $product): Event
    {
        return new \App\Infrastructure\Product\Event\ProductCreatedEvent($product);
    }
}
