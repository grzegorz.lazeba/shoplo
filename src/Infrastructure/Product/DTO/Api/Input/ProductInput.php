<?php
declare(strict_types=1);

namespace App\Infrastructure\Product\DTO\Api\Input;

use App\Application\Product\ICanCreateProduct;
use Symfony\Component\Validator\Constraints as Assert;

class ProductInput implements ICanCreateProduct
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min = 100
     * )
     */
    public string $description;

    /**
     * @Assert\NotBlank()
     */
    public float $priceValue;

    /**
     * @Assert\NotBlank()
     */
    public string $priceCurrency;

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getPriceValue(): float
    {
        return $this->priceValue;
    }

    public function getPriceCurrency(): string
    {
        return $this->priceCurrency;
    }
}
