<?php
declare(strict_types=1);

namespace App\Infrastructure\Product\DTO\Api\Output;

use App\Application\Product\ICanCreateProduct;
use App\Application\Product\ICanReadProduct;
use App\Domain\Product\Product;
use App\Infrastructure\Common\Util\DateHelper;

class ProductOutput implements ICanReadProduct
{
    public string $id;
    public string $description;
    public float $priceValue;
    public string $priceCurrency;
    public string $createdAt;

    public static function createFromEntity(Product $product): self
    {
        $self = new self();

        $product->buildReadModel($self);

        return $self;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function setPriceValue(float $priceValue): void
    {
        $this->priceValue = $priceValue;
    }

    public function setPriceCurrency(string $priceCurrency): void
    {
        $this->priceCurrency = $priceCurrency;
    }

    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt->format(DateHelper::DEFAULT_FORMAT);
    }
}
