<?php
declare(strict_types=1);

namespace App\Infrastructure\Product\DataFixture;

use App\Application\Product\CreateProductService;
use App\Application\Product\ICanCreateProduct;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    const DESCRIPTION
        = 'fixture_description_fixture_description_fixture_description_fixture_description_fixture_description_fixture
    description_fixture_description_fixture_description_fixture_description_fixture_description_fixture_description
    fixture_description_fixture_description_fixture_description_fixture_description_fixture_description_fixture
    fixture_description_fixture_description_fixture_description_fixture_description';

    const PRICE_CURRENCY = 'fixture_price_currency';

    const PRICE_VALUE = 1;

    private CreateProductService $productService;

    public function __construct(
        CreateProductService $productService
    ) {
        $this->productService = $productService;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 30; $i++) {
            $this->productService->create(
                new class implements ICanCreateProduct {
                    public function getDescription(): string
                    {
                        return ProductFixtures::DESCRIPTION;
                    }

                    public function getPriceValue(): float
                    {
                        return ProductFixtures::PRICE_VALUE;
                    }

                    public function getPriceCurrency(): string
                    {
                        return ProductFixtures::PRICE_CURRENCY;
                    }
                }
            );
        }
    }
}
