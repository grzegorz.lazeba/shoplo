<?php
declare(strict_types=1);

namespace App\Domain\Product;

use App\Application\Product\ICanReadProduct;

class Product
{
    private ProductId $id;

    private ProductDescription $description;

    private ProductPrice $price;

    private \DateTime $createdAt;

    private function __construct(
        ProductId $id,
        ProductDescription $description,
        ProductPrice $price
    ) {
        $this->id = $id;
        $this->description = $description;
        $this->price = $price;
        $this->createdAt = new \DateTime();
    }

    public static function createFrom(
        ProductId $productId,
        ProductDescription $productDescription,
        ProductPrice $productPrice
    ): self {
        return new self(
            $productId,
            $productDescription,
            $productPrice
        );
    }

    public function buildReadModel(ICanReadProduct $productReadModel): void
    {
        $productReadModel->setId($this->id->asString());
        $productReadModel->setDescription($this->description->asString());
        $productReadModel->setPriceCurrency($this->price->getCurrency());
        $productReadModel->setPriceValue($this->price->getValue());
        $productReadModel->setCreatedAt($this->createdAt);
    }

    public function getId(): string
    {
        return $this->id->asString();
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }
}
