<?php
declare(strict_types=1);

namespace App\Domain\Product;

use Assert\Assert;

final class ProductId
{
    private string $id;

    private function __construct(string $id)
    {
        Assert::that($id)->uuid();
        $this->id = $id;
    }

    public static function fromString(string $id): self
    {
        return new self($id);
    }

    public function asString(): string
    {
        return $this->id;
    }
}
