<?php
declare(strict_types=1);

namespace App\Domain\Product;

interface ProductRepositoryInterface
{
    public function nextIdentity(): ProductId;

    public function save(Product $product): void;
}
