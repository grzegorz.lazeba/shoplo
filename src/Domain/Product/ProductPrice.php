<?php
declare(strict_types=1);

namespace App\Domain\Product;

final class ProductPrice
{
    public float $value;

    public string $currency;

    private function __construct(float $value, string $currency)
    {
        $this->value = $value;
        $this->currency = $currency;
    }

    public static function createFrom(
        float $value,
        string $currency
    ): self {
        return new self($value, $currency);
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
