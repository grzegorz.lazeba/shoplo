<?php
declare(strict_types=1);

namespace App\Domain\Product;

use Assert\Assert;

final class ProductDescription
{
    private string $description;

    private function __construct(string $description)
    {
        Assert::that($description)->minLength(100);
        $this->description = $description;
    }

    public static function createFrom(string $description): self
    {
        return new self($description);
    }

    public function asString(): string
    {
        return $this->description;
    }
}
