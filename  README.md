# How to execute tests 
#### Step 1:
```
composer install
```
#### Step 2: Create .env.test file in main dir and add 

```
KERNEL_CLASS='App\Kernel'
DATABASE_URL=[urlToYourDatabase]
```

#### Step 3: Copy file phpunit.xml.dist and name it phpunit.xml. After that add in <php></php> section 

```
    <env name="URL_DOMAIN" value="YOUR_URL"/>
```
#### Step 4:
```
    php bin/console d:f:l --env=test
```

#### Step 5:
```
    php bin/phpunit
```
